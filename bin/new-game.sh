#!/bin/bash

echo What would you like your game to be called?

read -r GAMENAME

if [[ "$GAMENAME" == "*.*" ]]; then
  echo Game name cannot contain a period! Aborting...
  exit 1
fi

echo Initialising html directory...

FILENAME=$(echo $GAMENAME | tr " " "-" | tr '[:upper:]' '[:lower:]')

mkdir "src/main/html/$FILENAME"
cp -r "src/main/html/.template/." "src/main/html/$FILENAME/"

for FILE in ./src/main/html/$FILENAME/*; do
  echo "$FILE"
  sed -i "s/GAMENAME/$GAMENAME/g" "$FILE"
  sed -i "s/FILENAME/$FILENAME/g" "$FILE"
done
