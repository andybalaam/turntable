package com.heg.turntable.datasource

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class MergingJsonStoreTest {

    private val mapper = ObjectMapper()

    val JSON_A = """
        {
            "a": "1",
            "b": "2",
            "c": "3"
        }
    """

    val JSON_B = """
        {
            "d": "4",
            "e": "5",
            "f": "6"
        }
    """

    val JSON_C = """
        {
            "c": "4",
            "d": "5",
            "e": "6"
        }
    """

    @Test
    fun can_read_a_written_value_when_initial_map_is_empty() {
        val store = MergingJsonStore(FakeDataStore(), mapper)

        store.write("TEST", mapper.readTree(JSON_A))

        assertEquals(mapper.readTree(JSON_A), store.read("TEST"))
    }

    @Test
    fun combines_old_json_and_new_json_when_writing() {
        val store = MergingJsonStore(FakeDataStore(), mapper)

        store.write("TEST", mapper.readTree(JSON_A))
        store.write("TEST", mapper.readTree(JSON_B))

        assertEquals(
            mapper.readTree(
                """
                    {
                        "a": "1",
                        "b": "2",
                        "c": "3",
                        "d": "4",
                        "e": "5",
                        "f": "6"
                    }
                """
            ),
            store.read("TEST")
        )
    }

    @Test
    fun new_json_overwrites_old_json() {
        val store = MergingJsonStore(FakeDataStore(), mapper)

        store.write("TEST", mapper.readTree(JSON_A))
        store.write("TEST", mapper.readTree(JSON_C))

        assertEquals(
            mapper.readTree(
                """
                    {
                        "a": "1",
                        "b": "2",
                        "c": "4",
                        "d": "5",
                        "e": "6"
                    }
                """
            ),
            store.read("TEST")
        )
    }

    @Test
    fun json_merges_on_lower_levels() {
        val store = MergingJsonStore(FakeDataStore(), mapper)

        store.write("TEST", mapper.readTree(
            """
            {
                "a" : {
                    "aa" : 1,
                    "ab" : {
                        "aba" : "mamma mia",
                        "abb" : 3
                    }
                },
                "b" : "2"
            }  
            """
        ))

        store.write("TEST", mapper.readTree(
            """
            {
                "a" : {
                    "aa" : 2,
                    "ab" : {
                        "abb" : 4,
                        "abc" : "123"
                    }
                },
                "c" : "2"
            }  
            """
        ))

        assertEquals(
            mapper.readTree(
                """
                {
                    "a" : {
                        "aa" : 2,
                        "ab" : {
                            "aba" : "mamma mia",
                            "abb" : 4,
                            "abc" : "123"
                        }
                    },
                    "b" : "2",
                    "c" : "2"
                }  
                """
            ),
            store.read("TEST")
        )

    }

    @Test
    fun arrays_are_overwritten() {
        val store = MergingJsonStore(FakeDataStore(), mapper)

        store.write("TEST", mapper.readTree("""{"a" : [1, 2, 3] }"""))
        store.write("TEST", mapper.readTree("""{"a" : [3, 4, 5] }"""))

        assertEquals(
            mapper.readTree(
                """{"a" : [3, 4, 5] }"""
            ),
            store.read("TEST")
        )
    }

    @Test
    fun new_room_returns_a_random_room_string_from_characters_provided() {
        val store = MergingJsonStore(
            FakeDataStore(),
            mapper,
            "1234",
            4
        )

        val code = store.newRoom(mapper.readTree(JSON_A))

        assertEquals(4, code.length)
        code.forEach {
            assertTrue("1234".contains(it))
        }
    }

    @Test
    fun new_room_does_not_collide_with_existing_rooms() {
        val store = MergingJsonStore(
            FakeDataStore(
                mutableMapOf(
                    "A" to "json"
                )
            ),
            mapper,
            "AB",
            1
        )

        assertEquals("B", store.newRoom(mapper.readTree(JSON_A)))

    }
}

