package com.heg.turntable.games

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.heg.turntable.games.rules.Rule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GameRegistryTest {

    private val mapper: ObjectMapper = ObjectMapper()

    @Test
    fun initialisation_rules_are_applied() {
        val reg = GameRegistry(
            mapper,
            mapOf(
                "testGame" to listOf(IncrementRule())
            )
        )

        assertEquals(
            0,
            reg.initialiseJson(
                "",
                "testGame"
            )
                .get("writeCount")
                .asInt()
        )
        assertEquals(
            0,
            reg.initialiseJson(
                "{}",
                "testGame"
            )
                .get("readCount")
                .asInt()
        )
    }

    @Test
    fun x_www_urlencoding_is_decoded() {
        val reg = GameRegistry(
            mapper,
            mapOf(
                "testGame" to listOf(IncrementRule())
            )
        )

    }

    @Test
    fun read_rules_are_applied() {
        val reg = GameRegistry(
            mapper
        )

        assertEquals(
            mapper.readTree("""{ "key one" : "value one", "key two" : "value two" }"""),
            reg.initialiseJson("key%20one=value%20one&key%20two=value%20two", "someGame")
        )
    }

    @Test
    fun write_rules_are_applied() {
        val reg = GameRegistry(
            mapper,
            mapOf(
                "testGame" to listOf(IncrementRule())
            )
        )

        assertEquals(
            1,
            reg.applyWriteRules(
                mapper.readTree("""{"writeCount" : 0}"""),
                "{}",
                "testGame",
                "testUser"
            )
                .get("writeCount")
                .asInt()
        )
    }

    @Test
    fun multiple_rules_are_applied() {
        val reg = GameRegistry(
            mapper,
            mapOf(
                "testGame" to listOf(IncrementRule(), IncrementRule(), IncrementRule(), IncrementRule())
            )
        )

        assertEquals(
            4,
            reg.applyWriteRules(
                mapper.readTree("""{"writeCount" : 0}"""),
                "{}",
                "testGame",
                "testUser"
            )
                .get("writeCount")
                .asInt()
        )
        assertEquals(
            4,
            reg.applyReadRules(
                mapper.readTree("""{"readCount" : 0}"""),
                "testGame",
                "userId"
            )
                .get("readCount")
                .asInt()
        )
    }

}

class IncrementRule() : Rule {

    override fun initialiseMap(moveJson: ObjectNode): ObjectNode {
        moveJson.put("readCount", 0)
        moveJson.put("writeCount", 0)
        return moveJson
    }

    override fun alterRead(originalJson: ObjectNode, playerId: String): JsonNode {
        val readCount : Int = originalJson["readCount"].asInt() + 1
        originalJson.put("readCount", readCount)
        return originalJson
    }

    override fun alterWrite(originalJson: JsonNode, moveJson: ObjectNode, playerId: String): ObjectNode {
        val readCount : Int = if (moveJson.has("writeCount"))
            moveJson["writeCount"].asInt() + 1
        else
            1
        moveJson.put("writeCount", readCount)
        return moveJson
    }

}