package com.heg.turntable.games.rules.generic;

import com.fasterxml.jackson.databind.ObjectMapper
import com.heg.turntable.webserver.WebSocketException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test;

internal class PlayersTest {

    private val mapper = ObjectMapper()

    @Test
    fun map_is_initialised_with_empty_player_info() {
        assertEquals(
            mapper.readTree("""
                {
                    "players": {
                        "1" : null,
                        "2" : null
                    }
                    
                }
            """),
            Players(2, mapper).initialiseMap(mapper.createObjectNode())
        )
    }

    @Test
    fun secret_ids_are_removed_from_reads_except_for_your_id() {
        val players = Players(1, mapper)
        assertEquals(
            mapper.readTree("""
                {
                    "players": {
                        "1" : {
                            "name": null
                        },
                        "2" : {
                            "name": "name",
                            "secretId": "secret"
                        }
                    }
                    
                }
            """),
            players.alterRead(
                mapper.readTree("""
                {
                    "players": {
                        "1" : {
                            "name": null,
                            "secretId": "alsoSecret"
                        },
                        "2" : {
                            "name": "name",
                            "secretId": "secret"
                        }
                    }
                    
                }
            """).deepCopy(),
                "secret"
            )
        )
    }

    @Test
    fun when_game_is_not_full_you_cannot_edit_board() {
        try {
            Players(2, mapper).alterWrite(
                Players(2, mapper).initialiseMap(mapper.createObjectNode()),
                mapper.readTree(
                    """ { "board" : "changed" } """
                ).deepCopy(),
                ""
            )
            fail("Exception not thrown")
        } catch (e : WebSocketException) {
            assertEquals("Not enough players in the game", e.errorMessage)
        }
    }

    @Test
    fun when_game_is_full_you_can_edit_board() {
        assertEquals(
            mapper.readTree(
                """ { "board" : "changed" } """
            ).deepCopy(),
            Players(2, mapper).alterWrite(
                mapper.readTree("""
                    {
                    "players": {
                        "1": {
                          "name": "Liam",
                          "secretId": "woohoo"
                        },
                        "2": {
                          "name": "Foo",
                          "secretId": "woohoo2"
                        }
                    },
                    "board": "unchanged",
                    "turn": 0
                    }
                """),
                mapper.readTree(
                    """ { "board" : "changed" } """
                ).deepCopy(),
                "woohoo"
            )
        )
    }

    @Test
    fun when_game_is_not_full_you_can_add_yourself_as_a_player() {
        val moveJson = mapper.readTree(""" { "players" : {"1" : { "name" : "me", "secretId" : "a4xy2xa1"}} } """)
        assertEquals(
            moveJson,
            Players(2, mapper).alterWrite(
                Players(2, mapper).initialiseMap(mapper.createObjectNode()),
                moveJson.deepCopy(),
                "a4xy2xa1"
            )
        )
    }

    @Test
    fun once_a_player_has_been_registered_they_cannot_be_removed() {
        try {
            Players(2, mapper).alterWrite(
                mapper.readTree(""" { "players" : {"1" : { "name" : "me", "secretId" : "a4xy2xa1"}} } """),
                mapper.readTree(""" { "players" : {"1" : { "name" : "notme", "secretId" : "a4xy2xa2"}} } """).deepCopy(),
                "a4xy2xa1"
            )
            fail("No WebException thrown")
        } catch (e: WebSocketException) {
            assertEquals("You cannot overwrite an existing player", e.errorMessage)
        }
    }
}
