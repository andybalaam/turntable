package com.heg.turntable.sockets

import com.heg.turntable.datasource.MergingJsonStore
import com.heg.turntable.games.GameRegistry
import com.heg.turntable.webserver.WebSocketException
import io.ktor.http.cio.websocket.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

class SocketManager(
    private val jsonStore: MergingJsonStore,
    private val gameRegistry: GameRegistry,
    private val socketMap: MutableMap<String, MutableMap<String, DefaultWebSocketServerSession>> = HashMap()
) {
    private val log = LoggerFactory.getLogger(this.javaClass)

    suspend fun registerSocket(socket: DefaultWebSocketServerSession, roomId: String, gameName: String, userId: String) {
        try {
            if (roomId == "") {
                throw WebSocketException("You must have a room ID")
            }
            if (gameName == "") {
                throw WebSocketException("You must have a select a game")
            }
            if (userId == "") {
                throw WebSocketException("You must have a user ID")
            }
            if (!socketMap.containsKey(roomId)) {
                socketMap[roomId] = mutableMapOf(userId to socket)
            }
            if (socketMap.containsKey(roomId)) {
                log.info("Registering new socket to room $roomId")
                socketMap[roomId]!![userId] = socket
            }
            for (frame in socket.incoming) {
                try {
                    when (frame) {
                        is Frame.Text -> {
                            val data = frame.readText()
                            log.info("Recieved $data from $userId")
                            jsonStore.write(
                                roomId,
                                gameRegistry.applyWriteRules(
                                    jsonStore.read(roomId),
                                    data,
                                    gameName,
                                    userId
                                )
                            )

                            updateSockets(
                                roomId,
                                gameName
                            )
                        }
                        is Frame.Binary -> {
                            log.warn("Unexpected Binary Frame type. Closing Socket.")
                            socket.close()
                        }
                        is Frame.Close -> {
                            log.info("Socket closed.")
                            socket.close()
                        }
                        is Frame.Ping -> socket.send(Frame.Pong(frame.readBytes()))
                        is Frame.Pong -> {
                            log.warn("Unexpected Pong Frame type. Closing Socket.")
                            socket.close()
                        }
                    }
                } catch (e: WebSocketException) {
                    log.error("WebSocketException Caught - ${e.errorMessage}")
                    socket.send(e.json)
                }
            }
        } catch (e: WebSocketException) {
            log.error("WebSocketException Caught - ${e.errorMessage}")
            socket.send(e.json)
        }
    }

    private fun updateSockets(roomId: String, gameName: String) =
        if (socketMap.containsKey(roomId)) {
            socketMap[roomId]!!.forEach {
                val gameJson = gameRegistry.applyReadRules(jsonStore.read(roomId), gameName, it.key)
                GlobalScope.launch(Dispatchers.IO) {
                    it.value.send(Frame.Text(gameJson.toString()))
                }
            }
        }
        else {
            throw WebSocketException("No Connections found for this room!")
        }

}