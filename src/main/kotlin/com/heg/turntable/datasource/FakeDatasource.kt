package com.heg.turntable.datasource

class FakeDataStore(
    private val map: MutableMap<String, String> = HashMap()
) : Datasource {

    override fun write(roomId: String, moveJson: String) {
        map[roomId] = moveJson
    }

    override fun read(roomId: String): String = map.getOrDefault(roomId, "{}")

    override fun keyExists(roomId: String): Boolean = map.containsKey(roomId)

}