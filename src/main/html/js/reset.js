
function renderResetDiv(json) {
    const resetVoteCount = Object.keys(json["resetVotes"]).length
    const nonResetVoteCount = Object.keys(json["players"]).length - resetVoteCount

    if (document.getElementById("reset") == null) {
        var elem = document.createElement('div');
        elem.id = "reset"
        document.body.appendChild(elem);
    }

    document.getElementById("reset").innerHTML =
        '<button onclick="sendResetVote()" style="padding: 10px 10px; font-size: 75%;">Reset</button>' +
        '<span style="color: green"> &#10003;'.repeat(resetVoteCount) +
        '<span style="color: red"> &#10007;'.repeat(nonResetVoteCount)            
}

function sendResetVote() {
    const name = myName(gameJson)
    socket.send(
        JSON.stringify(
            { resetVotes : [ name ] }
        )
    ) 
}
