run-local:
	java -Dio.ktor.development=true -jar build/libs/turntable-all.jar env=local

run-heroku:
	java -jar build/libs/turntable-all.jar env=heroku

run-vm:
	java -jar build/libs/turntable-all.jar env=vm redisUrl=localhost webServerPort=8000

test:
	./gradlew test

jar:
	./gradlew shadowJar

new-game:
	bin/new-game.sh
